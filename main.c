#include "stm32f4xx_hal.h"
#include "core.h"
#include "usart.h"
#include "usart_comm.h"

static char buff[20];
static uint16_t ms_timer = 0;

/**
 * System Tick Interrupt Service Routine 
 */
void SysTick_Handler(void)
{
		ms_timer++;
	
		if(ms_timer>1000){					//every sec
			timer++;
			ms_timer = 0;
			if(timer_flag){
				char buff[30];
				sprintf(buff,"Counter value = %d\n\r",timer);
				USART_WriteString(buff);
			}
		}
       
		
		if (new_commend){
			size_t size =USART_ReadUntil(buff,'\n');
			if (size && buff[0] == '/'){
				USART_CommandHandler(buff, size);
			}
			new_commend = 0;
			USART_WriteString(">> ");
		}
} /* SysTick_Handler */



void LED_Init(void);




int main (void)
{
	HAL_Init();
	LED_Init();
	USART_Init();
	USART_WriteString("Hello USART\n\rType /help to get commend list\n\r");
	USART_WriteString(">> ");
	timer = 0;
	timer_flag = 0;
	while(1)
	{

		//put your code here
		//USART works in background
		
	}
}




void LED_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
 
  // GPIO Ports Clock Enable
  __GPIOG_CLK_ENABLE();
 

  GPIO_InitStruct.Pin   = GPIO_PIN_14 | GPIO_PIN_13;	//buint-in LEDs PG13, PG14
  GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;        // push-pull output
  GPIO_InitStruct.Pull  = GPIO_NOPULL;                // no pull
  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;             // analog pin bandwidth limited
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);
        
} /* LED_Init */

