#ifndef _USART_COMM_H_
#define _USART_COMM_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "stm32f4xx_hal.h"
#include <stdio.h>

// Timer value
extern unsigned int timer;
// Timer visibility flag
extern bool timer_flag;

/**
 * Executes USART commands 
 *
 * @param buff pointer to memory where command is stored
 * @param size of command
*/
void USART_CommandHandler(char *buff, size_t size);

#endif   //_USART_COMM_H_
