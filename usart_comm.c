#include "usart_comm.h"
#include <string.h>
#include "usart.h"

unsigned int timer;
bool timer_flag;


void USART_CommandHandler(char *buff, size_t size){
	
	if(!size) return;
	
	//Commands listed
	if(!memcmp(buff,"/help",size)){
		USART_WriteString("<<---------------HELP--------------->>\n\r/hello\t\tHello World\n\r/led_red_on\tTurn on red led\n\r/led_red_off\tTurn off red led\n\r\
		\r/led_green_on\tTurn on green led\n\r/led_green_off\tTurn off green led\n\r/time\t\tTime [ms] since reset\n\r/timer [value]\tChange timer value\n\r\
		\r/timer_on\tShow timer updates\n\r/timer_off\tHide timer updates\n\r<<---------------------------------->>\n\r");
	
	} else if(!memcmp(buff,"/led_red_on",size)){
		HAL_GPIO_WritePin(GPIOG,GPIO_PIN_14, GPIO_PIN_SET);
		
	}else if(!memcmp(buff,"/led_red_off",size)){
		HAL_GPIO_WritePin(GPIOG,GPIO_PIN_14, GPIO_PIN_RESET);
		
	}else if(!memcmp(buff,"/led_green_on",size)){
		HAL_GPIO_WritePin(GPIOG,GPIO_PIN_13, GPIO_PIN_SET);
		
	}else if(!memcmp(buff,"/led_green_off",size)){
		HAL_GPIO_WritePin(GPIOG,GPIO_PIN_13, GPIO_PIN_RESET);
		
	}else if(!memcmp(buff,"/hello",size)){
		USART_WriteString("Hello World\n\r");
		
	}else if(!memcmp(buff,"/time",size)){
		char buff[30];
		sprintf(buff,"Counter value = %d\n\r",timer);
		USART_WriteString(buff);
		
	}else if(size > 7 && !memcmp(buff,"/timer ",7)){
		int int_value = atoi(&buff[6]);
		timer = int_value;
		
	}else if(!memcmp(buff,"/timer_on",size)){
		timer_flag = 1;
		
	}else if(!memcmp(buff,"/timer_off",size)){
		timer_flag = 0;
	
	}else{
		USART_WriteString("INVALID COMMAND. Type /help to see available commands\n\r");
	}
	
}


