/* Includes ------------------------------------------------------------------*/
#include <assert.h>
#include "ring_buffer.h"


bool RingBuffer_Init(RingBuffer *ringBuffer, char *dataBuffer, size_t dataBufferSize) 
{
	assert(ringBuffer);
	assert(dataBuffer);
	assert(dataBufferSize > 0);
	
	if ((ringBuffer) && (dataBuffer) && (dataBufferSize > 0)) {
		ringBuffer->ring_size = dataBufferSize;
		ringBuffer->buff = dataBuffer;
		ringBuffer->head = ringBuffer->tail = ringBuffer->buff;
		ringBuffer->data_length = 0;
		return true;
	}
	
	return false;
}

bool RingBuffer_Clear(RingBuffer *ringBuffer)
{
	assert(ringBuffer);
	
	if (ringBuffer) {
		ringBuffer->head = ringBuffer->tail = ringBuffer->buff;
		ringBuffer->data_length = 0;					
		return true;
	}
	return false;
}

bool RingBuffer_IsEmpty(const RingBuffer *ringBuffer)
{
	assert(ringBuffer);	
	if (ringBuffer) {
		return !(ringBuffer->data_length);
	}
	return 0;
}

size_t RingBuffer_GetLen(const RingBuffer *ringBuffer)
{
	assert(ringBuffer);
	
	if (ringBuffer) {
		return (ringBuffer->data_length * (int)sizeof(char));
	}
	return 0;
	
}

size_t RingBuffer_GetCapacity(const RingBuffer *ringBuffer)
{
	assert(ringBuffer);
	
	if (ringBuffer) {
		return (ringBuffer->ring_size * (int)sizeof(char));
	}
	return 0;	
}


bool RingBuffer_PutChar(RingBuffer *ringBuffer, char c)
{
	assert(ringBuffer);
	
	if (ringBuffer) {
		if(ringBuffer->data_length == ringBuffer->ring_size) return false;	//full
		*(ringBuffer->head) = c;										
		if (ringBuffer->head == (ringBuffer->buff)+(ringBuffer->ring_size-1)) ringBuffer->head = ringBuffer->buff;	//back to first element
		else ringBuffer->head++;
		ringBuffer->data_length ++;

		return true;
	}
	return false;
}

bool RingBuffer_GetChar(RingBuffer *ringBuffer, char *c)
{
	assert(ringBuffer);
	assert(c);
	
  if ((ringBuffer) && (c)) {
		if(ringBuffer->data_length == 0) return false;	//empty
		*c = *(ringBuffer->tail);
		if (ringBuffer->tail == (ringBuffer->buff)+(ringBuffer->ring_size-1)) ringBuffer->tail = ringBuffer->buff;	//back to first element
		else ringBuffer->tail++;	  
		ringBuffer->data_length --;

		return true;
	}
	return false;
}

